---
layout: handbook-page-toc
title: "Engineering Internship Pilot Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Engineering Internship Pilot Program

GitLab is establishing an on-going internship program that can offer students, graduates, career switchers, and those returning to the workforce an opportunity to further their careers. We desire to create an inclusive program that will provide a fair and equal opportunity for all qualifying candidates.

We do not yet have a tried and tested program and are undertaking a limited pilot in 2020. This pilot will be used to validate and refine our approach to offering internships at an [all-remote company](/company/culture/all-remote/getting-started/). For this reason, the structure and scope of this program will likely change in the future.

### Responsibility

| Role                | Person             |
|---------------------|--------------------|
| Executive Sponsor   | Eric Johnson (VPE) |
| Program Lead (DRI)  | Roos Takken        |
| Recruiting Lead     | Liam McNally       |
| Program Coordinator | Jean du Plessis    |
| Program Coordinator | Nick Nguyen        |

### Candidate qualifying criteria
The following criteria are considered required for candidates to be eligible for the internship program:
1. Ruby on Rails or JavaScript framework (React, VueJS, AngularJS, Ember, etc.) programming experience
1. Contributor to open source projects
1. Available full-time during the internship
1. Able and willing to travel to the United States of America and the Netherlands for one week each
1. Able and willing to acclimate to, and operate in, an [all-remote environment](/company/culture/all-remote/getting-started/)
1. Interested in fulltime employment after the program (either immediately, or "next semester") - no upfront commitment to GitLab required

Please refer to the [Software Engineering Intern job family](/job-families/engineering/software-engineer-intern/) for further details. 

To apply for the internship please complete the [application form](https://boards.greenhouse.io/gitlab/jobs/4517303002). 

### Program Overview

#### Key dates leading up to internship

| Date                | Event              |
|---------------------|--------------------|
| 2019-11-12          | Applications for hosting an intern opens |
| 2019-11-13          | Applications for applying for internship opens |
| 2019-11-29          | Applications for hosting an intern closes |
| 2019-12-09          | Successful team applications announced |

#### Duration and timing
A priority for the pilot program is attracting promising university students who are nearing graduation. Therefore, we are choosing dates that align with university academic calendars.

The pilot program will be nine weeks starting 2020-06-15 until 2020-08-14.

#### Location
The internship program will primarily be [remote](/company/culture/all-remote/), with two weeks of co-location working.

1. Week 1: Intern Fast Boot - Amsterdam
    - The interns will gather in Amsterdam for kick-off and a week of onboarding
1. Weeks 2-15: Remote internship   
    - Interns will work remotely in their assigned team
1. Week 16: Intern send-off - San Francisco
    - The interns will co-locate for their final week in San Francisco to process their experience and socialize with each other
1. GitLab Contribute Invite
    - We will extend invites to the interns to attend Contribute in March 2020. 

#### Internship day-to-day activities
1. Week 1: Intern Fast Boot
    - The program coordinators and mentors will facilitate a pre-defined program. The aim is to onboard the interns, prepare them for their seven weeks of remote working, and set them up for success.
1. Weeks 2-15: Remote internship
    - An intern's daily schedule will generally reflect [how GitLab team members work](/company/culture/all-remote/getting-started/), which is to say we will not impose a rigid schedule. 
    - Interns will be encouraged to favor [async communication](/company/culture/all-remote/management/#asynchronous) and to [set their own work schedule](/handbook/values/#measure-results-not-hours). 
    - Mentors and program coordinators will provide coaching if an intern needs help in adjusting to remote work.
    - Interns will participate in the following pre-determined activities
        - Weekly [1:1](/handbook/leadership/1-1/) with their manager
        - Weekly 1:1 with a mentor
        - Weekly 1:1 with an internship program coordinator
        - Weekly intern [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats)
        - 2-3 group meetings per week moderated by a program coordinator.
        - Regular pair programming sessions with a mentor and other team members.
1. Week 16: Intern send-off
    - The program coordinators and mentors will facilitate a pre-defined program. The aim is to process the experience, gather feedback, and share learnings.

#### Compensation

Internships at GitLab offered in the framework described in this page will be paid and follow the same logic as that depicted in our [Compensation Calculator](https://about.gitlab.com/handbook/people-group/global-compensation/calculator/) and according to our [Global Compensation Principles](https://about.gitlab.com/handbook/people-group/global-compensation/). This means that, as usual, the San Francisco benchmark, location and experience factors will be taken into account during the recruitment process and before making an offer. Depending on country regulations, we will have to align with national labor laws.

### Recruitment 
For the pilot, we will primarily target our advertising and promotion of the program at university students. However, we are not limiting candidate intake to university students and are open to all qualifying candidates.
We exclusively focus all our active sourcing activities on candidates from [underrepresented groups](/handbook/incentives/#explanation-and-examples-of-underrepresented-groups).

#### Advertising
We advertise on all the traditional recruiting platforms as well as the GitLab vacancies page. Additional advertising is done on internship focussed job boards (e.g. Youtern, angel.co etc.)

#### University Recruitment
We proactively reach out to and engage with universities/colleges to hold a Virtual Careers Talk to encourage students at those universities to apply. Also, we reach out to any associations within these universities that represent underrepresented groups and hold Virtual Careers Talk with them.

We hold virtual talks at a minimum of five universities in the following regions: North America, Europe, APAC, MENA.

### Interviewing
We are setting the bar high at the application stage and introducing an automated online assessment to help with screening candidates.
These steps will allow us to work with a manageable number of candidates during the face-to-face interview stages.

#### Interview process
1. Comprehensive Application form including
    - Technical assignment
    - Situational questions rather than STAR, as candidates may not have any direct work/relatable experience.
    - Cover letter asking for candidates to specifically address defined areas.
1. Automated online technical assessment
1. Technical assessment interview
1. Behavioral/Values interview panel \*
    - Hiring manager
    - Recruiter/HR
    - Misc Interviewer

\* Candidates must score a strong yes to move on to the panel interview stage.

### Criteria for teams requesting an intern
GitLab teams are able to [apply for an intern](https://forms.gle/RgJGtsyDZcTriNuD7) to join their team.  

Applications get evaluated on the following criteria:
1. Mentorship
    - Does the team have a manager and 1-2 engineers willing to serve as mentors for the duration of the program?
    - Do the mentors have previous experience mentoring interns or junior engineers? Previous experience is a nice-to-have, but not a must-have.
1. Workload
    - Does the team have a roadmap containing low weight issues with few dependencies suitable for an intern?
1. Team Size and maturity
    - How established is this team? Will they be able to take on an intern without risking a decrease in velocity?     

The team manager and mentors will also need to be able to actively participate in the interview process.     

The Program Lead, Program coordinators, and VP of Engineering will evaluate the applications, and successful teams will be announced on 2019-12-09.
With the selection of hosting teams attributes of diversity (teams, locations, roles/function) will be taken into account. If it comes to a tie-break the VP of Engineering will be the DRI for the selection. 
