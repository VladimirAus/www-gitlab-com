---
layout: markdown_page
title: Support
description: "Visit the GitLab support page to find product support links and to contact the support team."
---

{:.no_toc}

GitLab offers a variety of support options for all customers and users on both paid
and free tiers. You should be able to find help using the resources linked below, regardless
of how you use GitLab. There are many ways to [contact Support](#contact-support), but
the first step for most people should be to [search our documentation](https://docs.gitlab.com).

If you can't find an answer to your question, or you are affected by an outage, then
customers who are in a **paid** tier should start by consulting our [statement of support](/support/statement-of-support.html) while being mindful of what is outside of the scope of support. Please understand that any support that might be offered beyond the scope defined there is done
at the discretion of the agent or engineer and is provided as a courtesy.

If you're using one of GitLab's **free** options, please refer to the
appropriate section for free users of either [self-managed GitLab](/support/statement-of-support.html#core-and-community-edition-users) or [on GitLab.com](/support/statement-of-support.html#free-plan-users).

Note that free GitLab Ultimate and Gold accounts granted through trials or as part of a grant for [educational institutions](https://about.gitlab.com/solutions/education/) or [open source projects](https://about.gitlab.com/solutions/open-source/) do not come with support. Support for open source and education accounts can, however, be purchased at a significant discount by [contacting Sales](https://about.gitlab.com/sales/).

## Contact Support
{:.no_toc}

| Plan | **Self-managed (hosted on your own site/server)** |
|------|----------------|
| Core |  [GitLab Community Forum](https://forum.gitlab.com) |
| Starter |  [Standard Support](https://support.gitlab.com) <br/> Reply within 24 hours 24x5
| Premium and Ultimate | [Priority Support](https://support.gitlab.com) <br /> Tiered reply times based on [definitions of support impact](#definitions-of-support-impact) |

| Plan | **GitLab.com** |
|------|----------------|
| Free |  [GitLab Community Forum](https://forum.gitlab.com) |
| Bronze |  [Standard Support](https://support.gitlab.com) <br/> Reply within 24 hours 24x5
| Silver and Gold | [Priority Support](https://support.gitlab.com) <br /> Tiered reply times based on [definitions of support impact](#definitions-of-support-impact) |

Ticket support is available in English, Japanese, Korean and Chinese. Please note that should you request a call, only English is available.

Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our [get help page](/get-help/).

## On This Page
{:.no_toc}

- TOC
{:toc}

## First time reaching support? 

Account setup can happen one of two ways: 

1.  Go to [support.gitlab.com](https://support.gitlab.com/hc/en-us/requests/new) and submit a new request. An account and password will be created for you. You will need to request a password reset and setup a new password before you can sign in. 
2.  Click on [Sign Up](https://gitlab.zendesk.com/auth/v2/login/registration?auth_origin=3252896%2Ctrue%2Ctrue&brand_id=3252896&return_to=https%3A%2F%2Fsupport.gitlab.com%2Fhc%2Fen-us&theme=hc) to create a new account using your company email address. Your account will be automatically associated to your GitLab account. 

You can keep track of all of your tickets and their current status using our support web interface! 
We recommend using the support web interface for a superior experience managing your tickets.

### Language Support

Ticket support is available in English, Japanese, Korean or Chinese. Should you be offered a call, only English is available.

## GitLab Support Service Levels

### Trials Support

Trial licenses do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or
SLA performance, please consider [contacting Sales](https://about.gitlab.com/sales/) to discuss options.

### Standard Support

Standard Support is included with all GitLab self-managed [Starter](/pricing/#self-hosted), and GitLab.com [Bronze plans](/pricing/#gitlab-com) while 'Next business day support' means you can expect a reply to your ticket within 24 hours 24x5. (See [Support Staffing Hours](#definitions-of-gitlab-support-hours)).

Please submit your support request through the [support web form](https://support.gitlab.com/).
When you receive your license file, you will also receive an email address to use if
you need to reach Support and the web form can't be accessed for any reason.


### Priority Support

Priority Support is included with all self-managed GitLab [Premium and Ultimate](/pricing/#self-managed) licenses, as well as GitLab.com [Gold and Silver](/pricing/#gitlab-com) plans. In particular, these plans receive **Tiered Support response times**:

| [Support Impact](#definitions-of-support-impact) | SLA  | Hours | How to Submit |
|-----------------|-------|------|------------------------------------------------|
| Emergency (Self-managed only) | 30 minutes | 24x7 | When your license file is sent to your licensee email addres, GitLab also sends a set of addresses to use to reach Support for emergency requests. You can also ask your Technical Account Manager for these addresses.<br/><br/> **Note:** In order to trigger an emergency page you *must* send a new email to the provided address. CCing the address on an existing thread will **NOT** page the on-call engineer. |
| Highly Degraded | 4 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/) or via the regular support email. |
| Medium Impact   | 8 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/) or via the regular support email. |
| Low Impact      | 24 hrs| 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/) or via the regular support email. |

Self-managed Premium and Ultimate also receive:

- **Support for High Availability (HA)**: A Support Engineer will work with your technical team around any issues encountered after an HA implementation is completed in cooperation with our Customer Success team.
- **Live upgrade assistance**: Schedule an upgrade time with GitLab. We'll host a live screen share session to help you through the process and ensure there aren't any surprises. Learn [how to schedule an upgrade.](scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance)

#### Definitions of Support Impact

- **Emergency** - Your instance of GitLab is unavailable or completely unusable (30 Minutes) 
  *A GitLab server or cluster in production is not available, or is otherwise unusable.* An emergency ticket can be filed and our On-Call Support Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.
   - **Note:** In order to trigger an emergency page you *must* send a new email to the provided address. CCing the address on an existing thread will **NOT** page the on-call engineer.
   - **Note:** For GitLab.com customers our infrastructure team is on-call 24/7 - please check [status.gitlab.com](https://status.gitlab.com) before contacting Support.
- **High** - GitLab is Highly Degraded (4 hours)
  *Significant Business Impact.* Important GitLab features are unavailable or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing; however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.
- **Medium** - Something is preventing normal GitLab operation (8 hours)
  *Some Business Impact.* Important GitLab features are unavailable or somewhat slowed, but a workaround is available. GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A Known bug impacts the use of GitLab, but a workaround is successfully being used as a temporary solution.
- **Low** - Questions or Clarifications around features or documentation or deployments (24 hours)
  *Minimal or no Business Impact.* Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab. Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.

#### Upgrading to Priority Support

Priority Support is automatically included on GitLab.com Silver and Gold plans as well as self-managed Premium and Ultimate plans. If your organization would like to upgrade to a plan with Priority Support, you can purchase it yourself [online](https://customers.gitlab.com), email your account manager, or email `renewals@gitlab.com`.

#### Service Level Agreement (SLA) details

- GitLab offers 24x5 support (24x7 for Priority Support Emergency tickets) bound by the SLA times listed above.
- The SLA times listed are the time frames in which you can expect the first response and each successive response.
- GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the SLA times are *not* to be considered as an expected time-to-resolution.

### Definitions of GitLab Support Hours

- **24x5** - GitLab Support Engineers are actively responding to tickets Sunday 3pm Pacific Time through Friday 5pm Pacific Time. 
- **24x7** - For [Emergency Support](#definitions-of-support-impact) there is an engineer on-call 24 hours a day, 7 days a week.

### Customer Satisfaction
24 hours after a ticket is Solved or automatically closed due to a lack of activity, a Customer Satisfaction survey will be sent out.
We track responses to these surveys through Zendesk with a target of 95% customer satisfaction.

Support Management regularly reviews responses, and may contact customers who leave negative reviews for more context.

### Phone Support
Inbound or on-demand calls are not currently a GitLab Support offering at any level. At times it may be necessary to escalate to a live troubleshooting session. While
you may request to escalate to a live call, the supporting engineer will make the determination if (a) the call is necessary and (b) if sufficient information has been
passed along to make the call successful.

As a result of a qualifying ticket, you will receive a link to schedule a call (or a direct link to a call in the case of Emergency support). Reuse of this link
is considered abuse of support, and calls scheduled in this way will be cancelled.

### Support for GitHost

Subscribers to GitHost receive next business day support via e-mail.

Please submit your support request through the [support web form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334487).

## General Support Practices

### Differences Between Support Tickets and GitLab Issues

It's useful to know the difference between a support ticket opened through our [support portal](https://support.gitlab.com) vs. [an issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues).

For customers with a license or subscription that includes support, always feel free to contact support with your issue in the first instance via [the support portal](https://support.gitlab.com). This is the primary channel the support team uses to interact with customers and it is the only channel that is based on an SLA. Here, the GitLab support team will gather the necessary information and help debug the issue. In many cases, we can find a resolution without requiring input from the development team. However, sometimes debugging will uncover a bug in GitLab itself or that some new feature or enhancement is necessary for your use-case.

This is when we create an [issue on GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues) - whenever input is needed from developers to investigate an issue further, to fix a bug, or to consider a feature request. In most cases, the support team can create these issues on your behalf and assign the correct labels. Sometimes we ask the customer to create these issues when they might have more knowledge of the issue or the ability to convey the requirements more clearly. Once the issue is created on GitLab.com it is up to the product managers and engineering managers to prioritize and drive the fix or feature to completion. The support team can only advocate on behalf of customers to reconsider a priority. Our involvement from this point on is more minimal.

#### We don't keep tickets open (even if the underlying issue isn't resolved)

Once an issue is handed off to the development team through an issue in a GitLab tracker, the support team will close out the support ticket as Solved _even if the underlying issue is not resolved_. This ensures that issues remain the single channel for communication: customers, developers and support staff can communicate through only one medium.

### Issue Creation

Building on the above section, when bugs, regressions, or any application behaviors/actions **not working as intended** are reported or discovered during support interactions, the GitLab Support Team will
create issues in GitLab project repositories on behalf of our customers.

For feature requests, both involving the addition of new features as well as the change of
features currently **working as intended**, support will request that the customer
create the issue on their own in the appropriate project repos.

### Working Effectively in Support Tickets

As best you can, please help the support team by communicating the issue you're facing,
or question you're asking, with as much detail as available to you. Whenever possible,
include:
- log files relevant to the situation.
- steps that have already been taken towards resolution.
- relevant environmental details, such as the architecture.

We expect for non-emergency tickets that GitLab administrators will take 20-30 minutes to formulate the support ticket with relevant information. A ticket without the above information will reduce the efficacy of support.

In subsequent replies, the support team may ask you follow-up questions. Please
do your best read through the entirety of the reply and answer any such questions.
If there are any additional troubleshooting steps, or requests for additional
information please do your best to provide it.

The more information the team is equipped with in each reply will result
in faster resolution. For example, if a support engineer has to ask for logs, 
it will result in more cycles. If a ticket comes in with everything required,
multiple engineers will be able to analyze the problem and will have what is
necessary to further escalate to developers if so required.

### Including colleagues in a support ticket

Zendesk will automatically drop any CCed email addresses for tickets that come in via email.
If you would like to include colleagues in a support interaction, there are two ways:

1. Ask in the ticket to have any number of addresses CCed. This will last for the duration of the ticket,
but if you open a new ticket you'll need to request additional participants be added again.
1. Ask your sales representative to submit a request on your behalf to allow others in your organization to view or comment on open support cases.
 
### Closing Tickets

If a customer explains that they are satisfied their concern is being addressed
properly in an issue created on their behalf, then the conversation should continue
within the issue itself, and GitLab support will close the support ticket. Should
a customer wish to reopen a support ticket, they can simply reply to it and it will
automatically be reopened.

### GitLab Instance Migration

If a customer requests assistance in migrating their existing self-hosted GitLab to
a new instance, you can direct them to our [Migrating between two self-hosted GitLab
Instances](https://docs.gitlab.com/ee/user/project/import/#migrating-between-two-self-hosted-gitlab-instances)
documentation. Support will assist with any issues that arise from the GitLab migration.
However, the setup and configuration of the new instance, outside of GitLab specific configuration,
is considered out of scope and Support will not be able to assist with any resulting issues.

## GitLab.com Specific Support Policies

### Account Recovery

If you have lost access to your account, perhaps due to having lost access to your
2FA device or the original email address that the account was set up with, the account
*may* be recovered provided the claimant can provide sufficient evidence of account
ownership. Use the [support web form](https://support.gitlab.com/) to request assistance.

Please note that in some cases reclaiming an account may be impossible. Read
["How to keep your GitLab account safe"](/blog/2018/08/09/keeping-your-account-safe/)
for advice on preventing this.

### Dormant Namespace Requests

The GitLab.com Support Team will consider a [namespace](https://docs.gitlab.com/ee/user/group/#namespaces) (user name or group name) to be "dormant" when the user has not logged in or otherwise used the namespace for an extended time.

User namespaces can be reassigned if both of the following are true:

1. The user's last sign in was at least two years ago.
2. The user is not the sole owner of any active projects.

Group namespaces can be reassigned if one of the following is true:

1. There is no data (no project or project(s) are empty).
2. The owner's last sign in was at least two years ago.

If the namespace contains data, GitLab Support will attempt to contact the owner over a two week period before reassigning the namespaces. If the namespace contains no data (empty or no projects) and the owner is inactive, the dormant namespaces will be released immediately.

Namespaces associated with unconfirmed accounts over 90 days old are eligible for immediate release. Group namespaces that contain no data and were created more than 6 months ago are likewise eligible for immediate release.

### Namespace & Trademarks

GitLab.com namespaces are available on a first come, first served basis and cannot be reserved. No brand, company, entity, or persons own the rights to any namespace on GitLab.com and may not claim them based on the trademark. Owning the brand "GreatCompany" does not mean owning the namespace "gitlab.com/GreatCompany". Any dispute regarding namespaces and trademarks must be resolved by the parties involved. GitLab Support will never act as arbitrators or intermediaries in these disputes and will not take any action without the appropriate legal orders.

### Log requests

Due to our [terms](/terms), GitLab Support cannot provide raw copies of logs. However, if users have concerns, Support can answer specific questions and provide summarized information related to the content of log files.

For paid users on GitLab.com, many actions are logged in the [Audit events](https://docs.gitlab.com/ee/administration/audit_events.html#group-events-starter) section of your GitLab.com project or group.

## US Federal Specific Support Policies
The unique requirements of US Federal Support mean there are some specific policies that apply.

### CCs are disabled
To help ensure that non-US citizens are not inadvertently included in a support interaction, the "CC" feature is disabled. As a result,
support personnel will be unable to add additional contacts to support tickets.

By request through your account manager, you may allow certain individuals in your organization the ability to view and respond to any open support tickets through the US Federal Support Portal.

## Further resources

Additional resources for getting help, reporting issues, requesting features, and
so forth are listed on our [get help page](/get-help/).
