---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-11-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 77        | 7.30%           |
| Based in EMEA                               | 299       | 28.34%          |
| Based in LATAM                              | 13        | 1.23%           |
| Based in NORAM                              | 666       | 63.13%          |
| **Total Team Members**                      | **1,055** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 760       | 72.04%          |
| Women                                       | 295       | 27.96%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,055** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 48        | 80.00%          |
| Women in Leadership                         | 12        | 20.00%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **60**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 369       | 82.74%          |
| Women in Development                        | 77        | 17.26%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **446**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.16%           |
| Asian                                       | 40        | 6.37%           |
| Black or African American                   | 22        | 3.50%           |
| Hispanic or Latino                          | 35        | 5.57%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 24        | 3.82%           |
| White                                       | 366       | 58.28%          |
| Unreported                                  | 140       | 22.29%          |
| **Total Team Members**                      | **628**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 17        | 9.44%           |
| Black or African American                   | 4         | 2.22%           |
| Hispanic or Latino                          | 11        | 6.11%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.89%           |
| White                                       | 108       | 60.00%          |
| Unreported                                  | 33        | 18.33%          |
| **Total Team Members**                      | **180**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 7         | 14.29%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 4.08%           |
| White                                       | 29        | 59.18%          |
| Unreported                                  | 11        | 22.45%          |
| **Total Team Members**                      | **49**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.09%           |
| Asian                                       | 92        | 8.72%           |
| Black or African American                   | 28        | 2.65%           |
| Hispanic or Latino                          | 52        | 4.93%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 32        | 3.03%           |
| White                                       | 576       | 54.60%          |
| Unreported                                  | 274       | 25.97%          |
| **Total Team Members**                      | **1,055** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 46        | 10.31%          |
| Black or African American                   | 7         | 1.57%           |
| Hispanic or Latino                          | 25        | 5.61%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 13        | 2.91%           |
| White                                       | 246       | 55.16%          |
| Unreported                                  | 109       | 24.44%          |
| **Total Team Members**                      | **446**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 7         | 11.67%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.67%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.33%           |
| White                                       | 34        | 56.67%          |
| Unreported                                  | 16        | 26.67%          |
| **Total Team Members**                      | **60**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.71%           |
| 25-29                                       | 199       | 18.86%          |
| 30-34                                       | 298       | 28.25%          |
| 35-39                                       | 213       | 20.19%          |
| 40-49                                       | 216       | 20.47%          |
| 50-59                                       | 100       | 9.48%           |
| 60+                                         | 10        | 0.95%           |
| Unreported                                  | 1         | 0.09%           |
| **Total Team Members**                      | **1,055** | **100%**        |


Source: GitLab's HRIS, BambooHR
