---
layout: markdown_page
title: "Category Direction - Container Registry"
---

- TOC
{:toc}

## Container Registry

The GitLab Container Registry is a secure and private registry for Docker images. Built on open source software, the GitLab Container Registry is completely integrated with GitLab.  Easily use your images with GitLab CI, create images specific for tags or branches and much more.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AContainer+Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

Our primary focus is on improving the performance and experience of untagging and deleting images from the Container Registry. We are prioritizing this work so that we can help our customers to lower the cost of storage for the Container Registry, improve the performance of the application and reduce the risk inherent to manually deleting images. 

Our highest priority is the epic [gitlab-#2272](https://gitlab.com/groups/gitlab-org/-/epics/2272), which will seek to optimize the Container Registry garbage collection algorithm. The goal of this epic will be to unblock our customers from deleting images from storage by decreasing the amount of down/read-only time required to run garbage collection. [gitlab-#26818](https://gitlab.com/gitlab-org/gitlab/issues/26818) will allow the process to run without requiring the Container Registry to be down or in read-only mode. 

[gitlab-#15398](https://gitlab.com/gitlab-org/gitlab/issues/15398) will introduce Docker tag expiration policies for all new projects and allow users to specify, using regex, which images they would expire and how frequently. This is the MVC of the larger epic, [gitlab-#2270](https://gitlab.com/groups/gitlab-org/-/epics/2270), which aims to add robust expiration and retention policies. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:
- [Improve the performance and reliability of garbage collection](https://gitlab.com/groups/gitlab-org/-/epics/2272) (In progress)
- [Storage tracking and limits for the Container Registry](https://gitlab.com/gitlab-org/gitlab-ce/issues/59232) (Not Started)
- [Implement in-line garbage collection](https://gitlab.com/gitlab-org/gitlab-ce/issues/57897) (Not Started)
- [Retention / expiration policy](https://gitlab.com/gitlab-org/gitlab-ce/issues/20247) (In Progress)
- [Fix Tag pruning and deletion logic](https://gitlab.com/gitlab-org/gitlab-ce/issues/21405) (Complete)
- [Sort images list](https://gitlab.com/gitlab-org/gitlab-ce/issues/20216)
- [Filter images list](https://gitlab.com/gitlab-org/gitlab-ce/issues/62309)

## Competitive Landscape

[JFrog](https://jfrog.com/artifactory/) and [Sonatype](https://www.sonatype.com/nexus-repository-sonatype) both offer support for building and deploying Docker images. Open source container registries such as [Docker Hub](https://hub.docker.com/) and Red Hat's [Quay](https://quay.io/) offer users a single location to build, analyze and distribute their container images. 

JFrog integrates with several different [CI servers through dedicated plug-ins](https://www.jfrog.com/confluence/display/RTF/Build+Integration), including Jenkins and Azure DevOps, but does not yet support GitLab. However, you can still connect to your Artifactory repository from GitLab CI. Here is an example of how to [deploy Maven projects to Artifactory with GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html).

GitLab provides an improved experience by being the single location for the entire DevOps Lifecycle, not just a portion of it. We will provide many of the features expected of a container registry, but without the weight and complexity of a single-point solution. 

### In the news (Q4 2020)
- November 2019: GitHub launched a [package registry](https://help.github.com/en/github/managing-packages-with-github-packages), including support for a [Docker container registry](https://help.github.com/en/articles/configuring-docker-for-use-with-github-package-registry), which allows users to authenticate, publish and install images. 
- November 2019: [Sonatype was acquired by Vista Equity Partners](https://techcrunch.com/2019/11/18/sonatype-acquired/).
- November 2019: [Docker acquired by Mirantis](https://www.zdnet.com/article/mirantis-acquires-docker-enterprise/).
- November 2019: JFrog announces their container registry is [now available in their community edition.](https://jfrog.com/jfrog-launches-free-artifactory-powered-jfrog-container-registry-to-empower-docker-and-helm-community/)
- November 2019: Red Hat announces that the [Quay Project is now open source](https://www.redhat.com/en/blog/red-hat-introduces-open-source-project-quay-container-registry).

## Top Customer Success/Sales Issue(s)

The top Customer Success / Sales issue is to improve the visibility and management layer of the Container Registry. The goal of [gitlab-ce#29639](https://gitlab.com/gitlab-org/gitlab-ce/issues/29639) is to improve the tracking and display of data to provide a more seamless user experience within GitLab. By completing this issue we will:
-  Allow  metadata to be stored and removed
-  Make it possible to easily track what data is stored in the registry
-  Make it possible to introduce retention policies for images stored in the registry


## Top Customer Issue(s)

The top customer issue is [gitlab-#31071](https://gitlab.com/gitlab-org/gitlab/issues/31071) which will unblock customers from running garbage collection. [gitlab-#26818](https://gitlab.com/gitlab-org/gitlab/issues/26818) will remove the requirement for down time and unblock all of our customers (and GitLb) from running garbage collection. 

There are additional top TAM issues identified which are popular amongst our customers:

- [GitLab Registry available images list/search](https://gitlab.com/gitlab-org/gitlab-ce/issues/26866)
- [Retention/expiration policy for container images](https://gitlab.com/gitlab-org/gitlab/issues/37242)

## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. [gitlab-#26818](https://gitlab.com/gitlab-org/gitlab/issues/26818) will allow the Infrastructure team to lower the total cost of the GitLab.com Container Registry by implementing in-line garbage collection and removal of blobs. 

## Top Vision Item(s)

We've learned from a recent [survey](https://gitlab.com/gitlab-org/ux-research/issues/328) and subsequent [user interviews](https://gitlab.com/gitlab-org/ux-research/issues/329), that users navigate to the Container Registry user interface for [one of three reasons](https://gitlab.com/gitlab-org/uxr_insights/issues/617).

- To look up which image or tag I should be using in my environment (32%)
- To verify my CI pipeline built the image as expected	(28%)
- To ensure my image was uploaded correctly (22%)

Our top vision item, [gitlab-#31636](https://gitlab.com/gitlab-org/gitlab/issues/31636) aims to add the required metadata and information to help users accomplish those tasks. 
