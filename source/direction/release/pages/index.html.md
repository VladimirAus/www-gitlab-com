---
layout: markdown_page
title: "Category Direction - GitLab Pages"
---

- TOC
{:toc}

## GitLab Pages

GitLab Pages allows you to create a statically generated website from your project that
is automatically built using GitLab CI and hosted on our infrastructure.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APages)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/user/project/pages/) 

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1296) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### Overall Prioritization

Pages is not strategically our most important Release feature, but it's a popular
feature and one that people really enjoy engaging with as part of the GitLab
experience; it's truly one of our most "personal" features in the Release stage.
We do not plan to provide a market-leading solution for static web page hosting,
but we do want to offer one that is capable for most basic needs, in particular
for hosting static content and documentation that is a part of your software
release. Popular issues and compelling blockers for our users hosting static content and documentation will be the top priority for Pages. 

## What's Next & Why
 
In order to improve pages performance, we are working on changing the [pages architecture](https://gitlab.com/groups/gitlab-org/-/epics/1316). 
Now that we have delivered the first improvement to speed up the initialization time ([gitlab#28782](https://gitlab.com/gitlab-org/gitlab/issues/28782)), 
our next focus will be to support Let's Encrypt more effectively in Pages ([gitlab#30660](https://gitlab.com/gitlab-org/gitlab/issues/30660), 
[gitlab#35608](https://gitlab.com/gitlab-org/gitlab/issues/35608)). We are working on validating supporting custom domains and will expect to have
a path to redirect to custom domains ([gitlab#14243](https://gitlab.com/gitlab-org/gitlab/issues/14243)) following the Pages re-architecture effort. 
Our most  popular issue for Pages, Multiple version Pages support ([gitlab#16208](https://gitlab.com/gitlab-org/gitlab/issues/16208)) 
will need to wait until the new architecture is in place, but we are working on an alternative solution of using environments for achieving the same 
functionality ([gitlab#33822](https://gitlab.com/gitlab-org/gitlab/issues/33822)). 

We are also actively working toward [deploying GitLab Pages on Kubernetes](https://gitlab.com/gitlab-org/gitlab/issues/39586), helping the Cloud Native Installation category reach a Complete maturity. 


## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Automatic certificate renewal](https://gitlab.com/gitlab-org/gitlab-foss/issues/28996) (Complete)
- [Access controls for Pages on gitlab.com](https://gitlab.com/gitlab-org/gitlab/issues/25362) (Complete)
- [Speed up Pages initialization time by using configuration API](https://gitlab.com/gitlab-org/gitlab/issues/28782) (Complete)
- [Pages without DNS wildcard](https://gitlab.com/gitlab-org/gitlab/issues/17584) (12.8)
- [Redirect from GitLab pages URL to custom domain when one exists](https://gitlab.com/gitlab-org/gitlab/issues/14243) (12.8)
- [Per-site, in-repo configuration](https://gitlab.com/gitlab-org/gitlab-pages/issues/57)
- [Multiple version support](https://gitlab.com/gitlab-org/gitlab/issues/16208)

## Competitive Landscape

Gitlab pages are offered as a service, but we do not position ourselves as a static
site market leader. Other providers, such as [Netlify](https://www.netlify.com/) provide
a more comprehensive solution. There are project templates available in Gitlab that offer
the use of  Netlify for static site CI/CD, while also still taking advantage of GitLab for
repos, merge requests, issues, and everything else ([gitlab-ce#57785)](https://gitlab.com/gitlab-org/gitlab-foss/issues/57785).

## Top Customer Issue(s) and Top Customer Success/Sales Issue(s)

The most popular customer issue is [gitlab#17584](https://gitlab.com/gitlab-org/gitlab/issues/17584).
Creating Gitlab pages today requires admins to setup wildcard DNS records and SSL/TLS certificates. Some services
and/or corporate security policies forbid wildcard DNS records, preventing users from
benefitting from using Gitlab pages. This issue will remove the need for wildcard DNS and allow
many more users to enjoy the pages experience.

## Top Internal Customer Issue(s)

Our top internal customer issue is [gitlab#16208](https://gitlab.com/gitlab-org/gitlab/issues/16208)
which enables having multiple GitLab Pages generated based on branches or tags.

Our own docs team is considering moving to a different hosting provider; details
on reasons why can be found at [gitlab-docs#313](https://gitlab.com/gitlab-org/gitlab-docs/issues/313).
The main difficulty with using Pages at [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) 
/ [GitLab documentation](https://gitlab.com/gitlab-org/gitlab-docs) scale is:

Caching artifacts between pages runs
 - We clear and recreate everything every time, but we could mark certain directories (mark certain folders as "do not rebuild", potentially detect what's different via only/except data), in which case these would just be persisted from the previous run. See comments in https://gitlab.com/gitlab-org/gitlab-foss/issues/29498 for how this might look.
 - The alternative is having your own server, where you can obviously just leave files there.

## Top Vision Item(s)

From a vision standpoint, adding Review Apps for Pages ([gitlab#16907](https://gitlab.com/gitlab-org/gitlab/issues/16907))
is interesting because it allows for more sophisticated development flows
involving testing, where at the moment the only environment that GitLab
understands is production. This would level up our ability for Pages to
be a more mission-critical part of projects and groups. Another vision item being investigated is to leverage JAMSTACK for Pages. The primary goal would be to enhance the user experience ([gitlab#2179](https://gitlab.com/groups/gitlab-org/-/epics/2179)) and allow easy to set up pages from the UI. 
