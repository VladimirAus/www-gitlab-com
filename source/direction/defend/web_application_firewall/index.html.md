---
layout: markdown_page
title: "Category Direction - Web Application Firewall"
---

- TOC
{:toc}

## Description
A web application firewall (or WAF) filters, monitors, and blocks HTTP traffic to and from a web application. A WAF is differentiated from a regular firewall in that a WAF is able to filter the content of specific web applications while regular firewalls serve as a safety gate between servers. By inspecting HTTP traffic, it can prevent attacks stemming from web application security flaws, such as SQL injection, cross-site scripting (XSS), file inclusion, and security misconfigurations.

### Goal

GitLab's goal with WAF is to provide visibility into your applications, clusters, and the traffic they receive. By being able to see what is being sent to your systems, GitLab wants to empower you to either block malicious traffic or to act on it somehow.

Additionally, we want to make it possible to identify and update parts of your app that are subject to malicious traffic. In this way, even if malicious traffic bypasses the WAF, you can ensure the underlying application itself is resilient against attacks like SQL injection or XSS.

### Roadmap
[Roadmap Board](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Adefend)

## What's Next & Why
Our next step is to [allow blocking mode for WAF](https://gitlab.com/gitlab-org/gitlab/issues/8558). This will ensure that any traffic identified as potentially malicious will be dropped before it reaches the application.

## Competitive Landscape
TODO

## Analyst Landscape

Gartner's [Magic Quadrant for Web Application Firewalls, September 2019](https://www.gartner.com/en/documents/3964460)
lists the following vendors:

- [Akamai](https://www.akamai.com/us/en/resources/application-firewall.jsp)
- [Alibaba Cloud](https://www.alibabacloud.com/product/waf)
- [Amazon Web Services](https://aws.amazon.com/waf/)
- [Barracuda Networks](https://www.barracuda.com/products/webapplicationfirewall)
- [Cloudflare](https://www.cloudflare.com/waf/)
- [F5 Networks](https://www.f5.com/products/security/advanced-waf)
- [Fortinet](https://www.fortinet.com/products/web-application-firewall/fortiweb.html)
- [Imperva](https://www.imperva.com/products/web-application-firewall-waf/)
- [Microsoft](https://docs.microsoft.com/en-us/azure/application-gateway/waf-overview)
- [Oracle](https://www.oracle.com/cloud/security/cloud-services/web-application-firewall.html)
- [Radware](https://www.radware.com/products/appwall/)
- [Signal Sciences](https://www.signalsciences.com/)

## Top Customer Success/Sales Issue(s)

There is no feature available for this category.

## Top Customer Issue(s)

The category is very new, so we still need to engage customers and get feedback about their interests and priorities in this area.

## Top Vision Item(s)
TODO


## Upcoming Releases
TODO
