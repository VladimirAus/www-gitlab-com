---
layout: job_family_page
title: "Support Management"
---

## Support Management

Like other Managers, Support Managers at GitLab see the team as their product. While they know how to communicate with customers their time is spent hiring a world-class team and putting them in the best position to succeed. They own the customer support experience and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Support Engineering Manager

As a Support Manager, you will be responsible for making sure that GitLab self-managed and GitLab.com customers and users receive an excellent support experience. 

GitLab Self-managed customers are a mix: everything from Fortune 500 companies in highly regulated environments, to government agencies to boutique development shops. 

GitLab.com is a product at scale: a million+ user product with customers all over the world, also supporting a wide variety of customer size and use cases. 

To do this you’ll be working closely with Support Engineers, GitLab.com Production, and Product teams to make sure we are listening to our customers and fixing (and preventing) problems. A desire to build efficient process while maintaining a friendly and helpful customer tone is essential. If you are excited about support and want to work in a team that values collaboration and results, this is the place for you.


A Support Engineering Manager will:
- Hire a world class team of support professionals by actively seeking and hiring globally distributed talent who are focused on delivering world class customer support
- Help Support staff level up in their skills and expertise
- Build efficient process to handle volume at scale
- Hold Regular 1:1s with all members on their team
- Drive team members to be self-sufficient
- Be an expert communicator; with customers, with Support team members and with the rest of GitLab
- Train Support staff to screen candidates and conduct interviews
- Improve the Customer Experience in measurable and repeatable ways
- Create a sense of psychological safety on their team
- Develop onboarding and training process for new Support staff
- Fully capable and excited to step into the role of a [Support Engineer](/job-families/engineering/support-engineer/) when needed.
- Develop and implement data-driven tactics to deliver on the strategic goals for Support, as set out on the overall team's [strategy](/company/strategy/) page as well as outlined in the [direction for Support](/handbook/support/#support-direction).
- Develop and use analytics to measure and improve the Support team
- Provide mentorship as needed to improve performance and job satisfaction
- Construct and monitor metrics to maintain customer and user SLA's
- Coordinate via GitLab Issues with other teams to drive GitLab feature development

#### Requirements

* 3 years or more experience in Managing people
* Experience with Zendesk or a comparable ticketing system
* Above average knowledge of SaaS Architecture and supporting such infrastructure
* Experience with ELK logging infrastructure
* Experience supporting and advocating for a million+ User base on a SaaS product.
* Experience with debugging Modern MVC Applications and git
* Affinity for (and experience with) providing customer support
* Excellent spoken and written English
* You share our [values](/handbook/values/), and work in accordance with those values
* [A technical interview](/handbook/hiring/interviewing/technical/) is part of the hiring process for this position.
* A customer scenario interview is part of the hiring process for this position.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

## Performance Indicators

Support Management have the following job-family performance indicators.

* [Customer satisfaction with Support](/handbook/support/#support-satisfaction-ssat)
* [Manage team within approved operating expenses](/handbook/support/#customer-support-operating-expenses)
* [Service Level Agreement](/handbook/support/#service-level-agreement-sla)
* [Maintain hiring plan and capacity to achieve IC:Manager ratio](/handbook/support/#individual-contributor-to-manager-ratio)
* [Develop a team of experts](/handbook/support/#increase-capacity--develop-experts)



#### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 90 minute technical interview with customer scenarios with a Support Engineer
* Candidates will then be invited to schedule a 45 minute interview with our Support Manager
* Candidates will then be invited to schedule an additional 45 minute interview with the VP of Engineering
* Finally, candidates may be asked to interview with the CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

### Director of Support

The Director of Support role extends the Support Engineering Manager Role

- Hire a world class team of managers and Support Engineers to work on their teams
- Help their managers and developers grow their skills and experience
- Manage multiple teams and projects
- Hold regular skip-level 1:1's with all members of their team
- Create a sense of psychological safety on their teams
- Drive technical and process improvements
- Draft and Report quarterly OKRs
- Own the support experience across all products
- Exquisite communication: Regularly achieve consensus amongst departments
- Represent the company publicly at conferences
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
