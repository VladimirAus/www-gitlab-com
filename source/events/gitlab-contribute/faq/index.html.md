---
layout: handbook-page-toc
title: "GitLab Contribute Frequently Asked Questions"
---

## Registration

#### How to get an invite as a New Hire? OR Managers - How does a new hire get registered?   
{:.no_toc}   
New team members are sent a Contribute registration email and TripActions invite on a weekly basis. New team members cannot register or book travel before their first day, which is when they get access to their GitLab email. The registration email and TripActions may already be in their inbox, or they will receive them within a few days of their first day. New team members will have two weeks to register and book their flights.   

##### Team members who have a signing date on or before 6 March 2020, will be eligible to attend Contribute 2020 so travel & accomodations can be arranged no less than two weeks prior to the event.
{:.no_toc}  

#### I am not sure I can attend just yet, what should I do? 
{:.no_toc}
You may have 30 days or less from the time you receive your registration email to register for Contribute. Your exact deadline time frame is listed in your registration invitation email. If you'd like to request an extention, please email `contribute@gitlab.com` with information on why you'd like more time to decide.

#### Do I need to book a ticket for the event? 
{:.no_toc}
Yes, we need to know you will be coming and when. Please do so sooner than later even if you think some details might change. It helps us plan for headcount and room allocations. 

#### What if I don't have a passport/need to renew my passport? 
{:.no_toc}
If you don't have a passport yet, please enter `000000000000` for now. Apply for a passport **ASAP**. When you have it come back to modify your registration and enter the number.

If you are renewing your passport, enter your current number and come back to modify it when you have the new passport.

If you need to get a passport or renew it *specifically* for traveling to Contribute, you can expense up to 75 USD to cover the cost of this process. If the costs are higher, GitLab will only reimburse up to 75 USD.

To take advantage of this perk, email `contribute@gitlab.com` with details for approval and include your manager in cc. Once approved, submit for expense reimbursement and mention that the expense has been approved.

#### How do I modify my registration? 
{:.no_toc}
There is a link to `Manage Registration Details` in the confirmation email sent after you register. 

#### How much free time will there be during Contribute? 
{:.no_toc}
You can count on having free time during Arrivals Day and in the evenings. You’ll have a packed schedule the rest of the time, including excursions, workshops, and keynotes. Please note that events are not mandatory, so if you’re not up for an excursion or a workshop, you’re not obligated to attend. However, the goal of Contribute is to build bonds with each other and cultivate a strong community, so if you decide not to attend an event, we hope that you spend that time recharging or getting to know others.

#### Can I stay at an Airbnb or book my own lodging? 
{:.no_toc}
All Contribute attendees must stay at the Contribute hotels. Airbnbs, other hotels, and other accommodations are not possible. This is both to ensure security of all attendees but also in order to get the most networking benefit from this company event.

#### How do I unregister from Contribute?
{:.no_toc}
We're sorry to hear you won't be able to join us. To unregister find the confirmation email sent after you registered, and click on `Manage Registration Details`, that will allow you to cancel your registration.

If you don't have access to that email, or if you need to unregister your SO as well, please email `contribute@gitlab.com` and the team will take care of it.

## Booking Travel

Please see the [booking travel page](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/booking-travel.md) for details related to budget and how to book your travel.

#### What if I can't find a flight within the budget for my region? 
{:.no_toc}
Please keep an eye on prices until you are about **2 weeks away from your booking deadline**. You can set up an alert to be notified in price changes on Google Flights. Please do searches outside of TripActions on Google Flights, airline websites, and other travel sites to see whether there's a price difference.

If you are still unable to find something that works, please reach out to `contributetravel@gitlab.com`, and we will do our best to accommodate you.

When you email the Contribute team, please attach screenshots of your research on both TripActions and external sites. **Please note that you must reach out to the Contribute team before booking your flight and gain approval for booking outside of your budget. Failure to do so may result in covering the overage out of pocket. Your manager will also be contacted in the event you book outside policy.**

#### I need an invitation letter from GitLab to apply for a visa. Where can I get one? Also, can I get one for my SO and/or children? 
{:.no_toc}
Please check the [handbook page on visas](https://about.gitlab.com/handbook/people-group/visas/#prague-contribute-2020-visa-invitation-letter) for details about getting an invitation letter.

#### What if I want to book my travel to/from a city other than Prague? 
{:.no_toc}
If you want to extend your trip to visit another city in the area, it's fine to book and expense your travel to/from another destination, provided it costs no more than traveling directly to/from Prague and falls within the budget for your region.

#### What if I want to extend my trip and travel on different days? 
{:.no_toc}
If you want to extend your trip, it's fine to book and expense your travel on different days, provided it costs no more than traveling directly to/from Prague and falls within the budget for your region.

#### When should I arrive/depart? 
{:.no_toc}
Plan to **arrive on March 22nd** (this means you mayu need to leave your home on the 21st). This is arrivals day and there are no planned activities for this day. Even if you arrive at 11:45pm you will not miss out on anything and the team will be there to check you in.

If you arrive early on the 22nd, you may not be able to check into your hotel room right away, but the team will be there to greet you, and the hotel can hold your luggage so you're free to explore the city.

**March 27th** is the departures day; you can leave at any time on that day as the event concludes the night before and no activities are planned for the 27th.

#### I found cheaper flights. Can I expense, or do I have to use TripActions? 
{:.no_toc}
If you find flights outside of TripActions that are cheaper, you can purchase and expense the flight, see [Booking travel outside TripActions](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/booking-travel.md#booking-travel-outside-tripactions).

#### Can I use the $300 upgrade for my flight? 
{:.no_toc}
The [handbook policy of $300 towards an upgrade on flights of 9+ hours](/handbook/spending-company-money) **does not apply to Contribute**.

However, if your travel time is 24+ hours or includes 3+ connections, please feel free to expense up to $300 towards an upgrade (i.e. $300 total, not per flight).

If you’re traveling 30+ hours, please let us know asap, and we can arrange for you to arrive a day earlier.

If you are taller than 1.95 m or 6'5", you can upgrade to Economy Plus. Please email `contributetravel@` before booking.

#### I need a Schengen visa for Contribute. What happens if my visa application is declined and I need to cancel my travel plans? 
{:.no_toc}
First of all, we hope this doesn't happen to you! But if it did, any cancellation fees would be taken care of by GitLab.

This is also applicable if you are unable to apply for visa before the deadline for booking flights. You can book your tickets before the deadline if you are unable to apply for a visa at that time. That way, you get better rates on your tickets. We can cancel the tickets based on your status of Visa later, and GitLab will take care of any cancellation fees.

#### I'd like to travel by train to Contribute but this won't be within budget for my region. What can I do? 
{:.no_toc}
Thanks for being eco-conscious! Please email `contributetravel@gitlab.com` with the price difference and we will do our best to accomodate you. 

#### What if I need to come early for a team meeting?
{:.no_toc}
Once your manager notifies the planning team at `contribute@gitlab.com` we'll make arrangements for lodging. 

#### Is checked luggage reimbursable? 
{:.no_toc}
On TripActions, most airlines provide an option to pay extra for checked luggage. GitLab will reimburse the cost for adding one checked bag at a standard weight (only if the existing booking doesn't permit checked luggage).   

If you booked outside of TripActions and the booking does not include any checked luggage, please feel free to expense the cost of addign one checked bag at a standard weight. If possible, please try to do so ahead of time, as it is often far more costly to do this when you reach the airport. 

## Room assignment
Please see the [room info page for details](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/room-info.md) related to assignments. 

#### When do I pay for a single room? 
{:.no_toc}
After the first wave of registration closes on December 1, 2019, we'll send invoices to those who have selected a single room. 

## Guests

#### Can I bring a guest? 
{:.no_toc}
Yes, GitLabbers may bring one (1) guest, and they're encouraged to buy a ticket to attend. This way they will be able to join in on all activities, meals, excursions, workshops, and more. Any guests that do not have a ticket will not be able to participate in any Contribute activity.

#### Can my guest/child attend without a ticket to Contribute? 
{:.no_toc}
This is discouraged for a few reasons. The purpose of Contribute is to connect with our team face to face, as we only gather in person once every nine months. If your guest is unable to join in on any activities, you may find yourself torn between spending time with other GitLabbers and hanging out with your guest, which could mean that you miss out on bonding with teammates.

It is also not guaranteed that you will get a private room if you request one (see the [room info page](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/room-info.md)). The safest way to being your SO to Contribute is to buy them a ticket. The cost of a private room to share with your guest is included in their ticket.

That said, we understand that Contribute is a long time away from your family, and if the only way to attend is to bring our child/guest/both, then we won't charge a guest ticket.

**Please note we cannot accommodate anyone under 18 at any Contribute activity, such as meals, excursions or workshops.**

#### How do I register my guest? 
{:.no_toc}
If you are buying a ticket for your guest for Contribute, you will need to register them separately. 

#### When is the latest I can register my guest to attend? 
{:.no_toc}
Your guest has the same deadline as you do. 

#### Is my guest's ticket refundable if they have do to cancel for some reason? 
{:.no_toc}
It is refundable up to **45 days before** Contribute. 

## Attending Contribute

#### Is it possible to attend Contribute for a shorter amount of time? 
{:.no_toc}
While we encourage everyone to attend the entire event because the program is crafted to ebb and flow with various learnings and differentiated interactions - both departmental and cross functional - we understand that this is a long time away from family and other priorities, so we're able to accept partial bookings as long as team members can **attend for at least 2 full days excluding Arrivals Day**.

#### If we have to leave early for a family emergency or something else, is it expected that we'll reimburse the company for the unused portion of the trip? What are general guidelines for this type of situation?
{:.no_toc}
In the case of an emergency, that requires you to leave Contribute, please reach out to an Ambassador. We'll help find transportation and provide any other support you need. You won't be expected to reimburse GitLab for anything.
